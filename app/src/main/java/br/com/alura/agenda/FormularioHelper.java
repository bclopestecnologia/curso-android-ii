package br.com.alura.agenda;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;

import br.com.alura.agenda.modelo.Aluno;

public class FormularioHelper {

    private final EditText campoNome;
    private final EditText campoEndereco;
    private final EditText campoTelefone;
    private final EditText campoSite;
    private final RatingBar campoNota;
    private final ImageView foto;
    private Aluno aluno;

    public FormularioHelper(FormularioActivity activity) {
        //Instancia os valores do campos
        campoNome = (EditText)activity.findViewById(R.id.et_formulario_nome);
        campoEndereco = (EditText)activity.findViewById(R.id.et_formulario_endereco);
        campoTelefone = (EditText)activity.findViewById(R.id.et_formulario_telefone);
        campoSite = (EditText)activity.findViewById(R.id.et_formulario_site);
        campoNota = (RatingBar)activity.findViewById(R.id.rb_formulario_nota);
        foto = activity.findViewById(R.id.iv_formulario_foto);
        aluno =  new Aluno();
    }

    public Aluno pegaAluno() {
        //Pega os valores dos campos do Aluno
        aluno.setNome(campoNome.getText().toString());
        aluno.setEndereco(campoEndereco.getText().toString());
        aluno.setTelefone(campoTelefone.getText().toString());
        aluno.setSite(campoSite.getText().toString());
        aluno.setNota(Double.valueOf(campoNota.getProgress()));
        aluno.setCaminhoFoto((String) foto.getTag());
        return aluno;
    }

    public void preencheFormulario(Aluno aluno) {
        //Preenche o fotmulário com os dados do aluno
        this.aluno = aluno;
        campoNome.setText(aluno.getNome());
        campoEndereco.setText(aluno.getEndereco());
        campoTelefone.setText(aluno.getTelefone());
        campoSite.setText(aluno.getSite());
        campoNota.setProgress(aluno.getNota().intValue());
        if (aluno.getCaminhoFoto() != null) {
            carregaFoto(aluno.getCaminhoFoto());
        }
    }

    public void carregaFoto(String caminhoDaFoto) {
        //Carrega a foto a ser mostrada na tela
        Bitmap bitmap = BitmapFactory.decodeFile(caminhoDaFoto);
        Bitmap bitmapReduzido = bitmap.createScaledBitmap(bitmap, 300, 300, true);
        foto.setImageBitmap(bitmapReduzido);
        foto.setScaleType(ImageView.ScaleType.FIT_XY);
        foto.setTag(caminhoDaFoto);
    }
}