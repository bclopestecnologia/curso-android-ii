package br.com.alura.agenda;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

import br.com.alura.agenda.dao.AlunoDAO;
import br.com.alura.agenda.modelo.Aluno;

public class FormularioActivity extends AppCompatActivity {

    public static final int CODIGO_CAMERA = 123;
    private FormularioHelper helper;
    private String caminhoDaFoto;
    //private ImageView campoFoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        //Instancia os campos
        helper = new FormularioHelper(this);

        final Intent intent = getIntent();
        Aluno aluno = (Aluno) intent.getSerializableExtra("aluno");

        //Verifica o aluno para preencher o formulário se for editar o aluno
        if(aluno != null) {
            helper.preencheFormulario(aluno);
        }

        Button bt_camera = (Button)findViewById(R.id.bt_formulario_foto);
        bt_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Tira a foto
                tiraFoto();
            }
        });
    }

    public void tiraFoto() {
        //Abre a camera para tirar a foto
        Intent vaiParaCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        caminhoDaFoto = getExternalFilesDir(null) + "/" + System.currentTimeMillis() + ".jpg";
        File arquivoFoto = new File(caminhoDaFoto);

        // Essa parte não funciona para o Android 7
        // vaiParaCamera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(arquivoFoto));
        // Essa parte muda no Android 7, estamos construindo uma URI
        // para acessar a foto utilizando o FileProvider
        vaiParaCamera.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", arquivoFoto));
        startActivityForResult(vaiParaCamera, CODIGO_CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Verifica se está tudo ok depois que tirou a foto
        if (requestCode == CODIGO_CAMERA && resultCode == RESULT_OK) {
            helper.carregaFoto(caminhoDaFoto);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Cria o menu com a opção de salvar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_formulario, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_formulario_ok:
                //Pega os valores dos campos preenchidos
                Aluno aluno = helper.pegaAluno();

                //Conecta com o banco de dados para adicionar o novo alunos
                AlunoDAO dao = new AlunoDAO(this);

                if (aluno.getId() == null) {
                    dao.insere(aluno);
                } else {
                    dao.altera(aluno);
                }
                dao.close();

                //Mostra a mensagem do nome do alunos que foi salvo
                Toast.makeText(FormularioActivity.this, "Aluno " + aluno.getNome() + " Salvo!", Toast.LENGTH_SHORT).show();
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
